package com.accenture.lkm.arrays;
import java.util.*;
public class ArrayDemo1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int num[] = new int[5];
		System.out.println("Length: "+num.length);
		/*num[0] = 10;
		num[1] = 20;
		num[2] = 30;
		num[3] = 40;
		num[4] = 50;*/
		int sum = 0;
		System.out.println("Enter the array elements");
		for(int i=0;i<num.length;i++) {
			num[i] = input.nextInt();
		}
		/*for(int no:num) {
			no = input.nextInt();
		}*/
		for(int i=0;i<num.length;i++) {
			sum = sum + num[i];
		}
		System.out.println("The sum is "+sum);
		input.close();
	}

}
