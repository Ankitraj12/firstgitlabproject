package com.accenture.lkm.arrays;
import java.util.*;
public class ArrayDemo2 {

	public static void main(String[] args) {
		int studmark[][] = new int[2][3];
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the input");
		for(int i=0;i<studmark.length;i++) {
			for(int j=0;j<studmark[i].length;j++) {
				studmark[i][j] = input.nextInt();
			}
		}
		for(int i=0;i<2;i++) {
			System.out.println("Student "+(i+1));
			for(int j=0;j<3;j++) {
				System.out.print(studmark[i][j]+"\t");
			}
			System.out.println();
		}
		input.close();
	}

}
