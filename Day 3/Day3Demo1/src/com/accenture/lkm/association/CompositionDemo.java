package com.accenture.lkm.composition;

public class CompositionDemo {

	public static void main(String[] args) {
		Customer customer = new Customer();
		customer.setCustId(90);
		customer.setCustName("Jai");
		System.out.println("Customer details");
		System.out.println(customer.getCustId()+" "+customer.getCustName());
		System.out.println("Account details");
		Account account = customer.getAccount();
		System.out.println(account.getAccNo()+" "+account.getAccBalance());
	}

}
