package com.accenture.lkm.composition;

public class Customer {
	private int custId;
	private String custName;
	private Account account;//Association
	Customer(){
		account = new Account();//Composition
		account.setAccNo(1001);
		account.setAccBalance(1000.0);
	}
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public Account getAccount() {
		return account;
	}

	
}
