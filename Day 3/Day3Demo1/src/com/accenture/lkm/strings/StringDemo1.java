package com.accenture.lkm.strings;

public class StringDemo1 {

	public static void main(String[] args) {
		String s = new String("Hello");
		String s1 = "Hello";
		String s2 = "Hello";
		String s3 = "Hello";
		String s4 = new String("Hello");
		String s5 = "Hi";
		
		//== reference
		System.out.println(s==s1);//F
		System.out.println(s1==s2);//T
		System.out.println(s3==s2);//T
		//equals
		System.out.println(s.equals(s1));
		System.out.println(s1.equals(s3));
		System.out.println(s2.equals(s1));
		System.out.println(s3.equals(s4));
		System.out.println(s4.equals(s5));
		
		String str1 = "Welcome";
		String str3 = str1;
		System.out.println(str1);
		String str2;
		str1 = "Bye";//Immutability
		str2 = str1;
		System.out.println(str1);
		System.out.println(str1);
		
		System.out.println(str2==str1);//true
		System.out.println(str2==str3);//false
		
		String d = "Hello";
		d = d.concat(" World");
		System.out.println(d);
		System.out.println(d.concat("!!"));
		System.out.println(d);
		System.out.println("Index of :" +indexOf(d));
	}

}
