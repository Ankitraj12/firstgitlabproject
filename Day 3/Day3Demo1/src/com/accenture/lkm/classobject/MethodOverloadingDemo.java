package com.accenture.lkm.classobject;
import java.util.Scanner;
public class MethodOverloadingDemo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Calc calc = new Calc();
		//calc.add(10.0, 10.0);
		//calc.add(10.0f, 10.0f);
		calc.add(input.nextInt(), input.nextInt());
		input.close();
	}

}
