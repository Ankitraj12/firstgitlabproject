package com.sample.handson2;

public class TestEmployee {

	public static void main(String[] args) {
		Employee e1= new Employee();
		Employee e2= new Employee(11,70000.00);
		Manager m1=new Manager();
		m1.setNumberOfReportees(23);
		e2.setFirstName(" Mukesh ");
		e2.setLastName(" Chabbra " );
		e1.printDetails();
		e2.printDetails();
		m1.printDetails();
		
	}

}
