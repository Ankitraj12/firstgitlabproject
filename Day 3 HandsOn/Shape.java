package com.sample.handson2;

public interface Shape {
	void draw();	
	
}
class Circle implements Shape{
	

	@Override
	public void draw() {
		System.out.println("Drawing the Circle");
	}
	void rotate() {
		System.out.println("Rotating the Circle");
	}
	
}
class Triangle implements Shape{
	void calculatePerimeter() {
		System.out.println("Calculating the Perimeter of the Triangle");
	}

	@Override
	public void draw() {
		System.out.println("Drawing the Triangle");
		
	}
	
}