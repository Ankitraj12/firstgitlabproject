package com.sample.handson2;

public class Employee extends Person
{
	private int empId;
	private double salary;
	public Employee()
	{
		this.empId=123;
		this.salary=50000;
	}
	public Employee(int empId,double salary)
	{
		this.empId=empId;
		this.salary=salary;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public void printDetails()
	{
		System.out.println("---Employee Details----");
		System.out.println(getFirstName()+" "+getLastName()+" "+getEmpId()+" "+getSalary());
	}
	

}
