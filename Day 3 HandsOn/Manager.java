package com.sample.handson2;

public class Manager extends Employee {
	private int numberOfReportees;
	public int getNumberOfReportees() {
		return numberOfReportees;
	}

	public void setNumberOfReportees(int numberOfReportees) {
		this.numberOfReportees = numberOfReportees;
	}
	public void printDetails()
	{
		System.out.println(getFirstName()+" "+getLastName()+" "+getEmpId()+" "+getSalary()+" "+getNumberOfReportees());
	}

}
